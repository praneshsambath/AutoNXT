import { Component, ViewChild } from '@angular/core';
import { LoadingController, Nav, Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { ValvePage } from '../pages/valves/valve';
import { LoginPage } from '../pages/Login/login';
import { MotorPage } from '../pages/Setting/Setting';
import { reportPage } from '../pages/report/report';
import { ModePage } from '../pages/mode/mode';
import { HomePage } from '../pages/home/home';
import { LogoutPage } from '../pages/Logout/logout';
import { AppManual } from '../pages/Manual Settings Tab/tab/manual';
import { AppNotifyReport } from '../pages/notify_report/notify_report';
// import { AppSplashScreen } from '../pages/splashscreen/splash';






@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{ icon: string, title: string, component: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, private splashScreen: SplashScreen, public loadingCtrl: LoadingController, public modalctrl: ModalController) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'home', title: 'Home', component: HomePage },
      { icon: 'magnet', title: 'Add Valves', component: ValvePage },
      { icon: 'cash', title: 'Device Modes', component: ModePage },
      { icon: 'construct', title: 'General Setting', component: AppManual },
      { icon: 'settings', title: 'Motor Setting', component: MotorPage },
      { icon: 'list-box', title: 'Valve Report', component: reportPage },
      { icon: 'paper', title: 'Notification Report', component: AppNotifyReport },
      { icon: 'man', title: 'LogOut', component: LogoutPage },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      // this.splashScreen.show();
      // let splash = this.modalctrl.create(AppSplashScreen);
      // splash.present;
    });
  }

  presentLoadingIos(page) {
    let loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: 'Processing...',
      duration: 1000
    });


    loading.present();
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
