import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { LoginPage } from '../pages/Login/login';
import { ValvePage } from '../pages/valves/valve';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NewValvePage } from '../pages/addNewValve/newValve';
import { HttpModule } from '@angular/http';
import { MotorPage } from '../pages/Setting/Setting';
import { reportPage } from '../pages/report/report';
import { ModePage } from '../pages/mode/mode';
import { HomePage } from '../pages/home/home';
import { AppVerifyOTP } from '../pages/OTP/otp';
import { LogoutPage } from '../pages/Logout/logout';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { AppManual } from '../pages/Manual Settings Tab/tab/manual';
import { AppEmergency } from '../pages/Manual Settings Tab/emergency/emergency';
import { ListPage } from '../pages/Manual Settings Tab/list/list';
import { AppNotifyReport } from '../pages/notify_report/notify_report';


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    NewValvePage,
    ValvePage,
  ListPage,
    MotorPage,
    ModePage,
    reportPage,
    AppVerifyOTP,
    LogoutPage,
    AppManual,
    AppEmergency,
    AppNotifyReport
  ],
  imports: [
    BrowserModule, HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    ValvePage,
    NewValvePage,
    ListPage,
    MotorPage,
    ModePage,
    reportPage,
    AppVerifyOTP,
    LogoutPage,
    AppManual,
    AppEmergency,
    AppNotifyReport
  ],
  providers: [
    StatusBar,
    SplashScreen, LocalNotifications,
    { provide: ErrorHandler, useClass: IonicErrorHandler }, LoginPage, NewValvePage, ListPage, MotorPage, ModePage, ValvePage, HomePage,]
})
export class AppModule {


  cus_name: any = localStorage.getItem('cus_name');

}
