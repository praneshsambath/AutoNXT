import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { ModeService } from './mode.service';


@Component({
    selector: 'Mode-Page',
    templateUrl: 'mode.html',
    providers: [ModeService]
})
export class ModePage {

    phase1: any = true;
    phase2: any = false;
    idTrue: any = true;
    idTrue1: any = false;
    autoMode: any;
    mannualMode: any;
    sequenceToggle: any;
    timerToggle: any;
    selectedSection: any = {};
    Valve1: any = [];
    devId: any;
    durData: any;
    userCondition: any = false;
    newValve: any = true;
    device_id: any;
    device_ids: any;
    cus_id: any;
    valves = [];
    mannualModeStaus: any;
    autoModeStatus: any;
    auto_mode_sequence: any;
    auto_mode_timer: any;
    manualModesValue = [];

    setValve = [
        {
            "cus_id": '',
            "device_id": [],
            "sequence": 0,
            "valves": [],
            "duration": ''
        }
    ]
    valvesData: any = [{ name: '01', selected: false }, { name: '02', selected: false }, { name: '03', selected: false }, { name: '04', selected: false }, { name: '05', selected: false },
    { name: '06', selected: false }, { name: '07', selected: false }, { name: '08', selected: false }, { name: '09', selected: false }, { name: '10', selected: false },
    { name: '11', selected: false }, { name: '12', selected: false }, { name: '13', selected: false }, { name: '14', selected: false }, { name: '15', selected: false },
    { name: '16', selected: false }, { name: '17', selected: false }, { name: '18', selected: false }, { name: '19', selected: false }, { name: '20', selected: false },
    { name: '21', selected: false }, { name: '22', selected: false }, { name: '23', selected: false }, { name: '24', selected: false }, { name: '25', selected: false },
    { name: '26', selected: false }, { name: '27', selected: false }, { name: '28', selected: false }, { name: '29', selected: false }, { name: '30', selected: false },
    { name: '31', selected: false }, { name: '32', selected: false }
    ];
    durations = ['00:30:00', '01:00:00', '01:30:00', '02:00:00', '02:30:00', '03:00:00',
        '03:30:00', '04:00:00', '04:30:00', '05:00:00', '05:30:00', '06:00:00',
        '06:30:00', '07:00:00', '07:30:00', '08:00:00', '08:30:00', '09:00:00',
        '09:30:00', '10:00:00', '10:30:00', '11:00:00', '11:30:00', '12:00:00',
        '12:30:00', '13:00:00', '13:30:00', '14:00:00', '14:30:00', '15:00:00',
        '15:30:00', '16:00:00', '16:30:00', '17:00:00', '17:30:00', '18:00:00',
        '18:30:00', '19:00:00', '19:30:00', '20:00:00', '20:30:00', '21:00:00',
        '21:30:00', '22:00:00', '22:30:00', '23:00:00', '23:30:00', '24:00:00'];

    ngOnInit() {


        this.getDeviceId();

        this.ModeServices.getMannualModeValue(this.cus_id, this.device_ids).subscribe(

            data => {
                if (data.length > 0) {

                    var smple = data[0].valves[0].split(",");
                    for (var j = 0; j < this.valvesData.length; j++) {
                        for (var k = 0; k < smple.length; k++) {
                            if (this.valvesData[j].name == smple[k]) {
                                this.valvesData[j].selected = true;

                            }
                        }
                    }
                    this.durData = data[0].duration;
                    this.getEnable(this.durData, this.valvesData);
                }
            },
            err => console.log(err)
        )




    }

    constructor(public navCtrl: NavController, public menuCtrl: MenuController, public ModeServices: ModeService) {
        // this.menuCtrl.enable(false, 'myMenu');
        this.device_ids = localStorage.getItem('dev_ids');
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));

    }
    getEnable = function (data1, data2) {
        if (data1 != '' && data2 != '') {

            this.newValve = false;
        }

    }
    getDeviceId() {

        this.device_ids = localStorage.getItem('dev_ids');
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        this.device_ids = localStorage.getItem('dev_ids');
        this.ModeServices.getAutoModeTypeStaus(this.cus_id, this.device_ids).subscribe(

            data => {
                if (data[0].auto_mode_sequence == 1) {

                    this.sequenceToggle = true;
                    this.timerToggle = false;
                } else {
                    this.timerToggle = true;
                    this.sequenceToggle = false;
                }
            },
            err => console.log(err)
        )
        this.setValve[0].cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        this.ModeServices.getCurrentDeviceStaus(this.cus_id).subscribe(

            data => {
                this.device_id = data[0].current_device;
                this.device_ids = localStorage.setItem('dev_ids', data[0].current_device);
                this.ModeServices.getMannualAutoModeStaus(this.device_id).subscribe(

                    data => {
                        if (data.length > 0) {
                            if (data[0].auto_mode == 1) {

                                this.autoMode = true;
                            } else {
                                this.mannualMode = false;
                            }
                            if (data[0].manual_mode == 1) {
                                this.mannualMode = true;
                                this.idTrue = true;
                                this.idTrue1 = false;

                            } else {
                                this.autoMode = false;
                                this.idTrue = false;
                                this.idTrue1 = true;
                            }
                        }
                    },
                    err => console.log(err)
                )
            },
            err => console.log(err)
        )


    }
    onSelectManual = function (data) {

        if (data == false) {

            this.idTrue = false;
            this.idTrue1 = true;
            this.autoMode = true;
            this.mannualMode = false;
        } else {

            this.idTrue = true;
            this.idTrue1 = false;
            this.autoMode = false;
            this.mannualMode = true;
        }

    }
    onSelectAutoMode = function (data) {

        if (data == true) {

            this.autoModeStatus = 1;
            this.mannualModeStaus = 0;
        } else {
            this.autoModeStatus = 0;
            this.mannualModeStaus = 1;
        }
        if (data == true) {
            this.ModeServices.currentAutoUpdate(this.cus_id, this.device_id, this.mannualModeStaus, this.autoModeStatus).subscribe(

                data => {

                },
                err => console.log(err)
            )

        }
        if (data == false) {

            this.idTrue = true;
            this.mannualMode = true;
            this.idTrue1 = false;
            this.autoMode = false;

        } else {
            localStorage.setItem('autoModeStatus', data);
            this.idTrue = false;
            this.mannualMode = false;
            this.idTrue1 = true;
            this.autoMode = true;

        }


    }

    updateManualModesValue = function (Valve1, dur) {
        // for (var k = 0; k < this.valvesData.length; k++) {
        //     if ( this.valvesData[k].selected = true) {


        //     }
        // }
        // this.Valve1.name.push(Valve1);
        // console.log(this.Valve1.name.length);
        // if (this.Valve1.name.length == 0) {
        //     alert('Update Mode Successfully.....');

        // }
        // else {
        console.log(Valve1, dur);
        this.valves = [];
        this.manualModesValue = [];
        console.log(Valve1);

        // for (var i = 0; i < valvesData.length; i++) {
        //     if (valvesData[i].selected == true) {
        //         this.valves.push(valvesData[i].name);

        //     }


        // }
        console.log(this.Valve1.name);
        if (this.Valve1.name != undefined) {
            this.manualModesValue = [{
                cus_id: this.cus_id,
                device_id: localStorage.getItem('dev_ids'),
                valves: this.Valve1.name,
                duration: dur


            }];
            alert('Update Mode Successfully.....');
            this.ModeServices.updatemanualmode(this.manualModesValue).subscribe(

                data => {

                },
                err => console.log(err)
            )
        } else {
            alert('Update Mode Successfully.....');

        }

        // }

    }
    sequenceMode = function (data) {

        if (data == false) {

            this.timerToggle = true;
            this.sequenceToggle = false;
        } else {
            this.timerToggle = false;
            this.sequenceToggle = true;
        }

        if (data == true) {

            this.auto_mode_sequence = 1;
            this.auto_mode_timer = 0;
        } else {
            this.auto_mode_sequence = 0;
            this.auto_mode_timer = 1;
        }

        this.ModeServices.autoModeType(this.cus_id, this.device_id, this.auto_mode_sequence, this.auto_mode_timer).subscribe(

            data => {

            },
            err => console.log(err)
        )



    }
    timerMode = function (data) {

        if (data == false) {
            this.timerToggle = false;
            this.sequenceToggle = true;
        } else {

            this.timerToggle = true;
            this.sequenceToggle = false;
        }
        if (data == true) {

            this.auto_mode_sequence = 0;
            this.auto_mode_timer = 1;
        } else {
            this.auto_mode_sequence = 1;
            this.auto_mode_timer = 0;
        }

        this.ModeServices.autoModeType(this.cus_id, this.device_id, this.auto_mode_sequence, this.auto_mode_timer).subscribe(

            data => {

            },
            err => console.log(err)
        )

    }
    onSelectValve(data) {
        for (var j = 0; j < this.valvesData.length; j++) {
            for (var k = 0; k < data.length; k++) {
                if (this.valvesData[j].name == data[k]) {
                    this.valvesData[j].selected = true;
                    // this.valvesData.push(this.valvesData[j]);
                }
            }
        }
        if (this.Valve1.name != '' && this.durData != undefined) {
            this.newValve = false;
        }

        if (this.Valve1.name.length > 8) {
            this.userCondition = true;
            setTimeout(() => {
                this.userCondition = false;
            }, 2000)
            //this.userCondition=false;
        }
        if (this.Valve1.name.length > 8) {

            this.Valve1.name = this.Valve1.name.slice(0, -1);

        }
        //alert('Select Max 8 Valves !!!!');

    }
    onSelectDur(durData) {

        if (this.Valve1.name != '' && durData != undefined) {
            this.newValve = false;
        }

    }
    Clear = function () {

        this.Valve1 = [];
        this.durData = '';
        if (this.Valve1.name == '' && this.durData == '') {

            this.newValve = true;

        }


    }
}
