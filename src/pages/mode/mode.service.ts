import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class ModeService {

    // ip = "http://52.15.171.241:3111";
    // ip = "http://192.168.1.190:3111";
    ip = "http://103.14.120.213:3000";

    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }
    getCurrentDeviceStaus(cus_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getCurrentDevice?cus_id=" + cus_id).map(res => res.json());
    }
    // currentmodeUpdate(cus_id, device_id, mannualModeStaus, autoModeStatus): Observable<any> {

    //     return this.http.get(this.ip + "/api/homepage/modeUpdate?cus_id=" + cus_id + "&device_id=" + device_id + "&auto_mode=" + autoModeStatus + "&manual_mode=" + mannualModeStaus).map(res => res.json());
    // }
    currentAutoUpdate(cus_id, device_id, mannualModeStaus, autoModeStatus): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/modeUpdate?cus_id=" + cus_id + "&device_id=" + device_id + "&auto_mode=" + autoModeStatus + "&manual_mode=" + mannualModeStaus).map(res => res.json());
    }
    getMannualAutoModeStaus(device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/mode_status?device_id=" + device_id).map(res => res.json());
    }
    updatemanualmode(data): Observable<any> {
        
        return this.http.post(this.ip + "/api/homepage/updatemanualmode", data).map(res => res.json());
    }
    autoModeType(cus_id, device_id, auto_mode_sequence, auto_mode_timer): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/autoModeType?cus_id=" + cus_id + "&device_id=" + device_id + "&auto_mode_sequence=" + auto_mode_sequence + "&auto_mode_timer=" + auto_mode_timer).map(res => res.json());
    }
    getMannualModeValue(cus_id, device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getManualMode?cus_id=" + cus_id + "&device_id=" + device_id).map(res => res.json());
    }
    getAutoModeTypeStaus(cus_id, device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getAutoMode?cus_id=" + cus_id + "&device_id=" + device_id).map(res => res.json());
    }
}
