import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { LoginService } from './login.service';
import { AppVerifyOTP } from '../OTP/otp';
import { HomePage } from '../home/home';


@Component({
  selector: 'Login-Page',
  templateUrl: 'login.html',
  providers: [LoginService]
})
export class LoginPage {
  user_details = {};
  status: boolean = false;
  login = {

    mobileNum: ''

  };
  posts = [];

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public loginservice: LoginService) {
    // this.menuCtrl.enable(false, 'myMenu');


    var storage_login = localStorage.getItem("loggedIn");
    if (storage_login == 'true') {
      this.navCtrl.push(HomePage);
    }
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menuCtrl.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menuCtrl.enable(true);
  }

  Submit(login) {
    if (login.mobileNum.length == 10) {
      this.loginservice.submitService(login.mobileNum).subscribe(

        data => {
          this.user_details = data;
          this.navigateTo(this.user_details, login);
        },
        err => console.log(err)
      )
    } else {
      setTimeout(() => {
        this.status = true;
      }, 1000)
    }
  }

  navigateTo(value, number) {

    localStorage.setItem('mobileNumber', number.mobileNum);

    if (value.message == "success") {
      this.navCtrl.push(AppVerifyOTP);
    }
    else if (value.message == "fail") {
      setTimeout(() => {
        this.status = true;
      }, 1000)
    }
  }
}
