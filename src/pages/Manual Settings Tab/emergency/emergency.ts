import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { AppEmergencyService } from "./emergency.service";

@Component({

    selector: 'Emergency-stop',
    templateUrl: 'emergency.html',
    providers: [AppEmergencyService]
})

export class AppEmergency {

    device_id: any;
    customer_id: any;
    on: any; off: any;
    status: boolean;

    constructor(public navctrl: NavController, private service: AppEmergencyService) {

        this.device_id = localStorage.getItem('customer_device_id')
        this.customer_id = localStorage.getItem("cus_id")
    }

    device_On() {
        this.service.emergency_On(this.device_id, this.customer_id).subscribe(

            data => {
                this.on = data;
                // console.log(this.on);
                if (this.on[0].message == "On success") {
                    this.status = true;
                }
            },
            err => console.log(err)
        )
    }
    device_Off() {
        this.service.emergency_Off(this.device_id, this.customer_id).subscribe(

            data => {
                this.off = data;
                // console.log(this.off);
                if (this.off[0].message == "Off success") {
                    this.status = false;
                }
            },
            err => console.log(err)
        )
    }

}