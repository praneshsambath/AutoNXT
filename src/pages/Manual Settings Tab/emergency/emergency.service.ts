import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class AppEmergencyService {

    data: any
    // ip = "http://52.15.171.241:3111";
    // ip = "http://192.168.1.190:3111";
    ip = "http://103.14.120.213:3000";
    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }

    emergency_Off(device_id, customer_id): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/getforcestop?cus_id=" + customer_id + "&device_id=" + device_id).map(res => res.json());
    }
    emergency_On(device_id, customer_id): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/getforcestart?cus_id=" + customer_id + "&device_id=" + device_id).map(res => res.json());
    }
}