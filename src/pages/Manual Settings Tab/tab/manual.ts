import { Component, ViewChild } from "@angular/core";
import { NavController, NavParams } from "ionic-angular";
import { Tabs } from "ionic-angular/navigation/nav-interfaces";
import { AppEmergency } from "../emergency/emergency";
import { ListPage } from "../list/list";



@Component({

    selector: 'tabs',
    templateUrl: 'manual.html'
})

export class AppManual {

    header: any = "REPORT";
    @ViewChild('myTabs') tabRef: Tabs;


    constructor(public navCtrl: NavController, public navParams: NavParams) {

    }


    tab1 = ListPage;
    tab2 = AppEmergency;
}