import { Component } from '@angular/core';
import { LoadingController, NavController, NavParams } from 'ionic-angular';
import { settingService } from './list.service';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
  providers: [settingService]

})
export class ListPage {

  ngOnInit() {


    this.getDeviceId()

  }
  selectedItem: any;
  getdevices = [];
  DeviceStatusReport = [];
  noId: any = "true";
  deviceSatus = '';
  client_status = false;
  report = [];
  Valve1 = JSON.parse(localStorage.getItem('dev_id'));

  deviceIdData: any = [{ device_id: '102', selected: 'true' }, { device_id: '103', selected: 'true' }, { device_id: '104', selected: 'false' }, { device_id: '105', selected: 'false' }, { device_id: '106', selected: 'false' }];

  cus_id: any = localStorage.getItem('cus_ids');
  cus_name: any;

  idTrue: any = false;
  id: any;

  icons: string[];
  items: Array<{ title: string, note: string, icon: string }>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public settingServices: settingService, public loadingCtrl: LoadingController) {

    this.cus_name = localStorage.getItem('cus_name');
  }
  getDeviceId() {
    this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
    this.settingServices.getCurrentDeviceStaus(this.cus_id).subscribe(

      data => {
        this.Valve1 = data[0].current_device;
      },
      err => console.log(err)
    )
    this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
    this.settingServices.getDeviceIdService(this.cus_id).subscribe(

      data => {
        this.getdevices = data;
        this.selectDeviceId(this.Valve1);

      },
      err => console.log(err)
    )

  }

  selectDeviceId(Valve1) {
    this.noId = "false";
    this.id = Valve1;
    this.settingServices.getDevicereport(this.id, this.cus_id).subscribe(

      data => {
        this.DeviceStatusReport = data;
      },

      err => console.log(err)

    )
  }
  deviceSetting = function (data) {

    this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
    alert('Set Device Successfully !!!!!');
    this.settingServices.updateDeviceStatus(this.cus_id, data).subscribe(

      data => {

        alert('Set Device Successfully !!!!!');
      },
      err => console.log(err)
    )

  }

  setOnOff(idTrue) {

    if (idTrue == true) {
      this.deviceSatus = '1';
    } else {

      this.deviceSatus = '0';
    }
    this.settingServices.setDeviceStatus(this.cus_id, this.id, this.deviceSatus).subscribe(

      data => {
        this.getdevices = data;
      },
      err => console.log(err)
    )

  }
}
