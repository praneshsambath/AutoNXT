import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class newValveService {


    // ip = "http://52.15.171.241:3111";
    // ip = "http://192.168.1.190:3111";
    ip = "http://103.14.120.213:3000";
    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;

    }

    addValveService(setval): Observable<any> {

        return this.http.post(this.ip + '/api/homepage/setvalves', JSON.parse(setval)).map(res => res.json());
    }
    getDeviceIdService(data): Observable<any> {


        return this.http.get(this.ip + "/api/homepage/getdevices?cus_id=" + data).map(res => res.json())
    }
    getLastSequence(cus_id, Valve1): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getlastsequence?cus_id=" + cus_id + "&device_id=" + Valve1).map(res => res.json());
    }
    getCurrentDeviceStaus(cus_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getCurrentDevice?cus_id=" + cus_id).map(res => res.json());
    }
    getvalvereport(Valve1, cus_id): Observable<any> {


        return this.http.get(this.ip + "/api/homepage/valvereport?cus_id=" + cus_id + "&device_id=" + Valve1).map(res => res.json());
    }
    getTotalDuration(cus_id, device_id): Observable<any> {


        return this.http.get(this.ip + "/api/homepage/gettotalduration?cus_id=" + cus_id + "&device_id=" + device_id).map(res => res.json());
    }

}
