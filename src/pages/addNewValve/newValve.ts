import { Component } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';
import { ValvePage } from '../valves/valve';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { newValveService } from './newValve.service';
@Component({
    selector: 'NewValve-Page',
    templateUrl: 'newValve.html',
    providers: [newValveService]
})
export class NewValvePage {
    getdevices: any;
    userCondition: any = false;
    newValve: any = true;
    Valve1: any = [];
    cus_id: any;
    report = [];
    deviceCurrent = [];
    devId = JSON.parse(localStorage.getItem('dev_id'));
    durData: any;
    totalDuration: any;
    balanceDur: any;
    durationCondition = false;
    addCondition = false;
    setValve = [
        {
            "cus_id": '',
            "device_id": [],
            "sequence": 0,
            "valves": [],
            "duration": ''
        }
    ]

    valvesData: any = [{ name: '01', selected: 'false' }, { name: '02', selected: 'false' }, { name: '03', selected: 'false' }, { name: '04', selected: 'false' }, { name: '05', selected: 'false' },
    { name: '06', selected: 'false' }, { name: '07', selected: 'false' }, { name: '08', selected: 'false' }, { name: '09', selected: 'false' }, { name: '10', selected: 'false' },
    { name: '11', selected: 'false' }, { name: '12', selected: 'false' }, { name: '13', selected: 'false' }, { name: '14', selected: 'false' }, { name: '15', selected: 'false' },
    { name: '06', selected: 'false' }, { name: '17', selected: 'false' }, { name: '18', selected: 'false' }, { name: '19', selected: 'false' }, { name: '20', selected: 'false' },
    { name: '21', selected: 'false' }, { name: '22', selected: 'false' }, { name: '23', selected: 'false' }, { name: '24', selected: 'false' }, { name: '25', selected: 'false' },
    { name: '26', selected: 'false' }, { name: '27', selected: 'false' }, { name: '28', selected: 'false' }, { name: '29', selected: 'false' }, { name: '30', selected: 'false' },
    { name: '31', selected: 'false' }, { name: '32', selected: 'false' }
    ];

    durations = ['00:30:00', '01:00:00', '01:30:00', '02:00:00', '02:30:00', '03:00:00',
        '03:30:00', '04:00:00', '04:30:00', '05:00:00', '05:30:00', '06:00:00',
        '06:30:00', '07:00:00', '07:30:00', '08:00:00', '08:30:00', '09:00:00',
        '09:30:00', '10:00:00', '10:30:00', '11:00:00', '11:30:00', '12:00:00',
        '12:30:00', '13:00:00', '13:30:00', '14:00:00', '14:30:00', '15:00:00',
        '15:30:00', '16:00:00', '16:30:00', '17:00:00', '17:30:00', '18:00:00',
        '18:30:00', '19:00:00', '19:30:00', '20:00:00', '20:30:00', '21:00:00',
        '21:30:00', '22:00:00', '22:30:00', '23:00:00', '23:30:00', '24:00:00'];

    ngOnInit() {


        this.getDeviceId()

    }

    cusId = JSON.parse(localStorage.getItem('cus_ids'));
    constructor(public navCtrl: NavController, public menuCtrl: MenuController, public valveService: newValveService, public loadingCtrl: LoadingController) {
        // this.menuCtrl.enable(false, 'myMenu');

    }

    ionViewDidEnter() {
        // the root left menu should be disabled on this page
        this.menuCtrl.enable(false);
    }

    ionViewWillLeave() {
        // enable the root left menu when leaving this page
        this.menuCtrl.enable(true);
    }

    Cancel() {

        this.navCtrl.push(ValvePage);
    }
    getDeviceId() {
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        this.valveService.getCurrentDeviceStaus(this.cus_id).subscribe(

            data => {
                this.devId = data[0].current_device;
                localStorage.setItem('dev_ids', data[0].current_device);
                this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
                this.deviceCurrent = JSON.parse(localStorage.getItem('dev_ids'));
                this.valveService.getTotalDuration(this.cus_id, data[0].current_device).subscribe(

                    data => {
                        if (data.length != 0) {
                            localStorage.setItem('totalDuration', data[0].totalduration);
                            localStorage.setItem('balDuration', data[0].bal_duration);
                            console.log(data[0].totalduration);
                            console.log(data[0].bal_duration);

                        }



                    },
                    err => console.log(err)
                )

            },
            err => console.log(err)
        )



        this.setValve[0].cus_id = JSON.parse(localStorage.getItem('cus_ids'));

        this.valveService.getDeviceIdService(this.setValve[0].cus_id).subscribe(

            data => {
                this.getdevices = data;
                this.devId = data[0].device_id;
                this.selectDeviceId(this.devId);
            },
            err => console.log(err)
        )
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        let diviceIdDur = JSON.parse(localStorage.getItem('dev_ids'));
        this.valveService.getvalvereport(diviceIdDur, this.cus_id).subscribe(


            data => {
                this.report = data;
                console.log(this.report);
            },
            err => console.log(err)
        )

    }

    selectDeviceId(Valve1) {
        if (this.devId != '' && this.Valve1 != '' && this.durData != '') {
            this.newValve = false;
        }

        this.setValve[0].cus_id = JSON.parse(localStorage.getItem('cus_ids'));

        this.valveService.getLastSequence(this.setValve[0].cus_id, Valve1).subscribe(

            data => {
                if (data.length != 0) {
                    localStorage.setItem('lastSequence', data[0].sequence);
                } else {
                    localStorage.setItem('lastSequence', '0');
                }
            },
            err => console.log(err)
        )
    }



    addNewValve(data, data1, data2) {
        if (data1 != '') {
            this.totalDuration = localStorage.getItem('totalDuration');
            this.balanceDur = localStorage.getItem('balDuration');
            console.log(this.totalDuration);

            if (this.totalDuration <= "24:00:00" || this.totalDuration == "null") {

                this.navCtrl.push(ValvePage);
                let a = [];
                this.setValve[0].valves = data.name;
                this.setValve[0].duration = data1;
                this.setValve[0].sequence = JSON.parse(localStorage.getItem('lastSequence')) + 1;
                this.setValve[0].device_id.push(data2);
                this.setValve[0].cus_id = JSON.parse(localStorage.getItem('cus_ids'));

                for (var i = 0; i < data.length; i++) {
                    a.push(data[i].name);
                    this.setValve[0].valves = a;

                }

                let setval = JSON.stringify(this.setValve);

                //alert('Successfully Added !!!!!');

                this.navCtrl.push(ValvePage);
                this.valveService.addValveService(setval).subscribe(

                    data => {

                        alert('Successfully Added !!!!!');


                        localStorage.setItem('dev_id', data2);
                    },
                    err => console.log(err)
                )


            } else {

                this.balanceDur = localStorage.getItem('balDuration');
                this.durationCondition = true;
                setTimeout(() => {
                    this.balanceDur = this.balanceDur;
                    this.durationCondition = false;
                    this.newValve = true;
                    this.Valve1 = '';
                    this.durData = '';
                }, 1000)


            }
            // var j=0;
            // for(var i=0;i<this.report.length;i++){
            //     j=j+parseInt(this.report[i].duration);
            //     console.log(j);
            // }

        } else {

            this.addCondition = true;
        }

    }
    onSelectValve() {
        if (this.devId != '' && this.Valve1 != '' && this.durData != undefined) {
            if (this.durData != '') {
                this.newValve = false;
            } else {
                this.newValve = true;
            }
            if (this.Valve1.length == 0) {
                this.newValve = true;
            } else {
                this.newValve = false;
            }
        } else if (this.devId != '' && this.Valve1.length == 0 && this.durData != undefined) {

            this.newValve = true;
        }

        if (this.Valve1.length > 8) {
            this.userCondition = true;
            setTimeout(() => {
                this.userCondition = false;
            }, 2000)
        }
        if (this.Valve1.length > 8) {

            this.Valve1 = this.Valve1.slice(0, -1);

        }

    }


    onSelectDur(durData) {
        if (durData != '') {
            this.addCondition = false;
        }



        this.totalDuration = localStorage.getItem('totalDuration');
        this.balanceDur = localStorage.getItem('balDuration');
        if (this.totalDuration == "null") {
            this.newValve = false;
            this.durationCondition = false;

        } else {

            if (this.totalDuration <= "24:00:00") {
                if (this.balanceDur >= durData) {
                    this.balanceDur = localStorage.getItem('balDuration');
                    this.newValve = false;
                    setTimeout(() => {
                        this.balanceDur = this.balanceDur;
                        this.durationCondition = false;
                        this.newValve = false;

                    }, 1000)
                } else if (this.balanceDur < durData) {
                    if (durData != '') {

                        this.newValve = true;
                        this.durData = '';
                        this.durationCondition = true;
                    }
                    //else{

                    //     this.newValve = true;
                    // }
                }
            } else {


                this.newValve = false;
                this.durationCondition = false;
            }
        }

        // if (this.devId != '' && this.Valve1 != '' && durData != undefined) {
        //     this.newValve = false;
        // }

    }


}
