import { Component } from '@angular/core';
import { LoadingController, NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { NewValvePage } from '../addNewValve/newValve';
import { HomeValveService } from './valve.service';


@Component({
  selector: 'page-home',
  templateUrl: 'valve.html',
  providers: [HomeValveService]
})
export class ValvePage {


  timeStarts = '01:00';


  ngOnInit() {


    this.getDeviceId();
    this.presentLoadingIos();
    this.getManualTimeStatus();
    this.clientcheckStatus();

  }

  res = ["1", "2", "3"];
  cus_id: any;
  timePick: any = true;
  motor_switch: any = '';
  device_id: number;
  device_ids: any;
  deviceIdforTime: any;
  staiceSequence = 1;
  sequenceId: any;
  getdevices: any;
  report = [];
  idTrue: any = '';
  client_status = false;
  autoModeOn: any;
  selectedSection = {};
  autoModeOnValue: any;
  satrtTimeShow: any;
  Valve1 = JSON.parse(localStorage.getItem('dev_id'));
  arrayLength: boolean = true;
  arrayLength1: boolean = false;
  showContent: any;
  sequenceModeTime: any;
  sequenceToggle: any;
  deleteButton: any;

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public HomeValveService: HomeValveService, public loadingCtrl: LoadingController) {
    this.selectedSection = "tabButtonOne";
    this.menuCtrl.enable(true, 'myMenu');


  }

  presentLoadingIos() {
    let loading = this.loadingCtrl.create({
      spinner: 'ios',
      content: 'Please Wait ..........',
      duration: 1000
    });

    loading.present();
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

    this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
    this.HomeValveService.getCurrentDeviceStaus(this.cus_id).subscribe(

      data => {
        this.Valve1 = data[0].current_device;

      },
      err => console.log(err)
    )
    this.clientcheckStatus();
  }
  addNewValve() {
    this.navCtrl.push(NewValvePage);
  }
  getDeviceId() {

    this.autoModeOnValue = JSON.parse(localStorage.getItem('autoModeStatus'));
    console.log(this.autoModeOnValue)
    this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
    this.HomeValveService.getDeviceIdService(this.cus_id).subscribe(

      data => {
        this.getdevices = data;

        this.Valve1 = data[0].device_id;

        this.selectDeviceId(this.Valve1);
        this.device_ids = localStorage.getItem('dev_ids');
        this.HomeValveService.getMannualAutoModeStaus(this.device_ids).subscribe(

          data => {

            if (data[0].auto_mode == 1) {
              this.showContent = true;

            } else if (data[0].manual_mode == 1) {
              // this.showContent = false;
              this.showContent = true;
            }


          },
          err => console.log(err)
        )
        this.device_ids = localStorage.getItem('dev_ids');
        this.HomeValveService.getAutoModeTypeStaus(this.cus_id, this.device_ids).subscribe(

          data => {

            if (data.length > 0) {
              if (data[0].auto_mode_sequence == 1) {
                this.satrtTimeShow = false;

              } else if (data[0].auto_mode_timer == 1) {
                this.satrtTimeShow = true;
              }

            }
          },
          err => console.log(err)
        )

        this.HomeValveService.getSequenceTimeModeEnable(this.cus_id, this.device_ids).subscribe(

          data => {

            if (data.length > 0) {
              if (data[0].auto_mode_sequence_status == 1) {
                this.sequenceToggle = true;

              } else if (data[0].auto_mode_sequence_status == 0) {
                this.sequenceToggle = false;
              }
              // if (data[0].auto_mode_timer == 1) {

              //     this.timerToggle = true;
              // } else {
              //     this.sequenceToggle = false;
              // }
            }
          },
          err => console.log(err)
        )
      },
      err => console.log(err)
    )


  }
  sequenceMode = function (data) {

    if (data == true) {

      this.sequenceModeTime = 1;

    } else {

      this.sequenceModeTime = 0;
    }
    this.HomeValveService.setSequenceTimeModeEnable(this.cus_id, this.device_ids, this.sequenceModeTime).subscribe(

      data => {
        // this.report = data;
        // this.getDeviceId();

      },


      err => console.log(err)
    )

  }
  selectDeviceId(Valve1) {
    this.deviceIdforTime = Valve1;
    this.getManualTimeStatus()
    this.getStartTime();

    this.HomeValveService.getvalvereport(Valve1, this.cus_id).subscribe(

      data => {


        setTimeout(() => {
          this.HomeValveService.getClientcheckStatus(this.Valve1).subscribe(

            data => {

              if (data.length > 0) {
                if (data[0].client_connection_status == 1) {
                  this.client_status = false;
                  this.timePick = false;


                } else {
                  this.client_status = true;
                  this.timePick = true;
                  let loading = this.loadingCtrl.create({
                    spinner: 'dots',
                    content: 'Device OFF State..........',
                    duration: 1000
                  });

                  loading.present();
                }
              }

            },
            err => console.log(err)
          )


        }, 1000);
        this.report = data;
        for (var i = 0; i < data.length; i++) {
          if (data[i].valves == 0 && data[i].valves == 'null') {

            this.deleteButton = true;
          } else {
            this.deleteButton = false;
          }

        }

        if (data.length > 0) {
          this.arrayLength = false;
          this.arrayLength1 = true;
        } else {
          this.arrayLength = true;
          this.arrayLength1 = false;

        }

      },
      err => console.log(err)
    )
  }

  remove(sequenceId, Valve1) {
    localStorage.setItem('dev_id', Valve1);
    alert('Delete Succesfully !!!!!');
    this.navCtrl.push(ValvePage);

    this.sequenceId = sequenceId;
    this.device_id = Valve1;

    this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
    this.selectDeviceId(this.device_id);
    this.HomeValveService.deletevalve(this.cus_id, this.device_id, this.sequenceId).subscribe(

      data => {
        // this.report = data;
        // this.getDeviceId();

      },


      err => console.log(err)
    )
    this.navCtrl.push(ValvePage);
  }

  selectTime(deviceId, timeStarts) {


    this.HomeValveService.setTimeValue(this.cus_id, deviceId, timeStarts, this.staiceSequence).subscribe(

      data => {

      },
      err => console.log(err)
    )
  }


  getStartTime() {

    this.HomeValveService.getStartTimes(this.cus_id, this.deviceIdforTime, this.staiceSequence).subscribe(

      data => {

        if (data.length > 0) {
          this.timeStarts = data[0].start_time;

        }
      },
      err => console.log(err)
    )
  }
  setOnOff(idTrue) {

    if (idTrue == true) {

      this.timePick = false;
    } else {
      this.timePick = true;
    }

    if (idTrue == true) {
      this.motor_switch = '1';
    } else {

      this.motor_switch = '0';
    }
    this.HomeValveService.setTimeStatus(this.Valve1, this.motor_switch).subscribe(

      data => {
        this.getdevices = data;

      },
      err => console.log(err)
    )

  }
  getManualTimeStatus() {

    this.HomeValveService.getStartTimesStatus(this.Valve1).subscribe(

      data => {
        if (data[0].auto_mode == 1) {
          this.idTrue = true;
          this.timePick = false;
        } else {
          this.idTrue = false;
          this.timePick = true;
        }

      },
      err => console.log(err)
    )
  }


  clientcheckStatus() {

    setTimeout(() => {
      this.HomeValveService.getClientcheckStatus(this.Valve1).subscribe(

        data => {
          if (data.length > 0) {
            if (data[0].client_connection_status == 1) {
              this.client_status = false;
              this.timePick = false;


            } else {
              this.client_status = true;
              this.timePick = true;
              let loading = this.loadingCtrl.create({
                spinner: 'dots',
                content: 'Device OFF State..........',
                duration: 2000
              });

              loading.present();
            }
          }
        },
        err => console.log(err)
      )


    }, 2000);

  }



}
