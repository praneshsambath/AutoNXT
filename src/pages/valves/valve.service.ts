import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class HomeValveService {



    // ip = "http://52.15.171.241:3111";
    // ip = "http://192.168.1.190:3111";
    ip = "http://103.14.120.213:3000";
    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }
    getDeviceIdService(data): Observable<any> {


        return this.http.get(this.ip + '/api/homepage/getdevices?cus_id=' + data).map(res => res.json());
    }

    deletevalve(cus_id, device_id, sequenceId): Observable<any> {


        return this.http.get(this.ip + "/api/homepage/deletevalve?cus_id=" + cus_id + "&device_id=" + device_id + "&sequence=" + sequenceId + "&=").map(res => res.json());
    }
    getvalvereport(Valve1, cus_id): Observable<any> {


        return this.http.get(this.ip + "/api/homepage/valvereport?cus_id=" + cus_id + "&device_id=" + Valve1).map(res => res.json());
    }
    setTimeValue(cus_id, deviceId, timeStarts, staiceSequence): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/timeupdation?cus_id=" + cus_id + "&device_id=" + deviceId + "&sequence=" + staiceSequence + "&time=" + timeStarts).map(res => res.json());
    }
    getStartTimes(cus_id, deviceId, staiceSequence): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getDeviceStartTime?cus_id=" + cus_id + "&device_id=" + deviceId + "&sequence=" + staiceSequence).map(res => res.json());
    }
    setTimeStatus(deviceId, deviceSatus): Observable<any> {


        return this.http.get(this.ip + "/api/homepage/automode?device_id=" + deviceId + "&status=" + deviceSatus).map(res => res.json());
    }
    getStartTimesStatus(deviceId): Observable<any> {


        return this.http.get(this.ip + "/api/homepage/automode_status?device_id=" + deviceId).map(res => res.json());
    }
    getClientcheckStatus(deviceId): Observable<any> {


        return this.http.get(this.ip + "/api/homepage/clientcheck?device_id=" + deviceId).map(res => res.json());
    }
    getCurrentDeviceStaus(cus_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getCurrentDevice?cus_id=" + cus_id).map(res => res.json());
    }
    getMannualAutoModeStaus(device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/mode_status?device_id=" + device_id).map(res => res.json());
    }
    getAutoModeTypeStaus(cus_id, device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getAutoMode?cus_id=" + cus_id + "&device_id=" + device_id).map(res => res.json());
    }
    setSequenceTimeModeEnable(cus_id, device_id, sequenceTime): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/sequencetimeupdation?cus_id=" + cus_id + "&device_id=" + device_id + "&sequence_status=" + sequenceTime).map(res => res.json());
    }
    getSequenceTimeModeEnable(cus_id, device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getsequencestatus?cus_id=" + cus_id + "&device_id=" + device_id).map(res => res.json());
    }
}
