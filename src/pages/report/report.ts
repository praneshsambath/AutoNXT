import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { ReportService } from './report.service';
import { LocalNotifications } from '@ionic-native/local-notifications';


@Component({
    selector: 'Report-Page',
    templateUrl: 'report.html',
    providers: [ReportService]

})
export class reportPage {

    details: any;
    Valve1 = JSON.parse(localStorage.getItem('dev_id'));
    devidsForSeq: any;
    login = {
        mobileNum: ''
    };
    cus_id: any;
    report = [];
    getdevices = [];
    arrayLength: boolean = true;
    arrayLength1: boolean = false;
    lastSequence = [];

    // Valve1=JSON.parse(localStorage.getItem('dev_id'));
    ngOnInit() {
        this.getDeviceId();
        //this. selectDeviceId(JSON.parse(localStorage.getItem('dev_id')));
    }
    constructor(public navCtrl: NavController, public menuCtrl: MenuController, public reportService: ReportService, public notify: LocalNotifications) {
        // this.menuCtrl.enable(false, 'myMenu');
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
    }

    getDeviceId() {
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        this.reportService.getCurrentDeviceStaus(this.cus_id).subscribe(

            data => {
                this.Valve1 = data[0].current_device;
                this.selectDeviceId(this.Valve1);
            },
            err => console.log(err)
        )

        this.reportService.getDeviceIdService(this.cus_id).subscribe(

            data => {
                this.getdevices = data;
                this.Valve1 = data[0].current_device;


            },
            err => console.log(err)
        )


    }


    selectDeviceId(Valve1) {


        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));

        this.reportService.getvalvereport(Valve1, this.cus_id).subscribe(

            data => {
                this.report = data;
                if (data.length > 0) {

                    this.arrayLength = false;
                    this.arrayLength1 = true;
                } else {
                    this.arrayLength = true;
                    this.arrayLength1 = false;

                }

            },
            err => console.log(err)
        )
   }

}
