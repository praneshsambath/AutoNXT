import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AppVerifyOTP } from '../OTP/otp';
import { logOutService } from './logout.service';


@Component({
  selector: 'logout-Page',
  templateUrl: 'logout.html',
  providers: [logOutService]
})
export class LogoutPage {
  user_details = {};
  status: boolean = false;
  login = {

    mobileNum: ''

  };
  posts = [];

  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public loginservice: logOutService) {


    localStorage.setItem("loggedIn", 'false');



  }

  ionViewDidEnter() {
    // the root left menu should be disabled on this page
    this.menuCtrl.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving this page
    this.menuCtrl.enable(true);
  }

  Submit(login) {
    if (login.mobileNum.length == 10) {
      this.loginservice.submitService(login.mobileNum).subscribe(

        data => {
          this.user_details = data;
          this.navigateTo(this.user_details, login);
        },
        err => console.log(err)
      )
    } else {
      setTimeout(() => {
        this.status = true;
      }, 1000)
    }
  }

  navigateTo(value, number) {

    localStorage.setItem('mobileNumber', number.mobileNum);

    if (value.message == "success") {
      this.navCtrl.push(AppVerifyOTP);
    }
    else if (value.message == "fail") {
      setTimeout(() => {
        this.status = true;
      }, 1000)
    }
  }
}
