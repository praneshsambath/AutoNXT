import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class settingService {


    // ip = "http://52.15.171.241:3111";
    // ip = "http://192.168.1.190:3111";
    ip = "http://103.14.120.213:3000";
    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }
    getCurrentDeviceStaus(cus_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getCurrentDevice?cus_id=" + cus_id).map(res => res.json());
    }
    set2PhaseValue(cus_id, device_id, data): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/updateParam2phase?cus_id=" + cus_id + "&device_id=" + device_id + "&low_volt=" + data.lowVolt + "&high_volt=" + data.highVolt + "&dry_run_cur=" + data.dryRun + "&over_load_cur=" + data.overLoad).map(res => res.json());
    }
    set3PhaseValue(data): Observable<any> {


        return this.http.post(this.ip + "/api/homepage/updateParam3phase", data).map(res => res.json());
    }
    setScannerTimer(data): Observable<any> {


        return this.http.post(this.ip + "/api/homepage/scannertimer", data).map(res => res.json());
    }
    getscannertimer(cus_id, device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getscannertimer?cus_id=" + cus_id + "&device_id=" + device_id).map(res => res.json());
    }
    getParam2phase(cus_id, device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getParam2phase?cus_id=" + cus_id + "&device_id=" + device_id).map(res => res.json());
    }

    getParam3phase(cus_id, device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getParam3phase?cus_id=" + cus_id + "&device_id=" + device_id).map(res => res.json());
    }
}
