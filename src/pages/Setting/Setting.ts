import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { settingService } from '../Setting/Setting.service';

@Component({
    selector: 'Motor-Page',
    templateUrl: 'Setting.html',
    providers: [settingService]
})
export class MotorPage {

    dry_run: boolean = false;

    motorSetting: any = false;
    userSetting: any = true;
    phase1: any = true;
    phase2: any = false;
    new2Phase: any = false;
    SavePhase2: any;
    SavePhase3: any;
    SaveUserSet: any;
    lowVoltageArray: any = [];
    highVoltageArray = [];
    phase2DryRunArray = [];
    differVoltageArray = [];
    phase3DifferCurArray = [];
    highVoltage3phaseArray = [];
    lowVoltage3phaseArray = [];
    faultCt = ['N', 'R', 'Y', 'B', 'A'];
    lowVoltageScanArray = [];
    dryRuncanArray = [];
    ScanDryRestart = [];
    getScanArray = [];
    cus_id: any;
    device_id: any;
    phase2_par = {

        lowVolt: '',
        highVolt: '',
        dryRun: '',
        overLoad: ''
    };
    phase2_3Phasepar = {

        lowVolt: '',
        highVolt: '',
        differVolt: '',
        dryCur: '',
        dryLoad: '',
        differCur: '',
        faultCt: ''

    }
    user = {
        cus_id: '',
        device_id: '',
        dryScan: '',
        overLoad: '',
        lowVolt: '',
        startDelta: '',
        startCapacit: '',
        autoStart: '',
        dryRun: '',
        dryRestarts: ''
    }
    Phase3par = [
        {
            cus_id: "",
            device_id: "",
            low_volt: "",
            high_volt: "",
            diff_volt: "",
            dry_run_cur: "",
            over_load_cur: "",
            diff_cur: "",
            fault_ct: ""

        }]
    userScanTimer = [
        {
            cus_id: "",
            device_id: "",
            dry_run_scan_time: "",
            over_load_scan_time: "",
            volt_scan_time: "",
            start_delta_time: "",
            start_capacitor_time: "",
            auto_start_time: "",
            dry_run_restart: "",
            dry_run_restart_nos: ""

        }]
    selectedSection: any = {};

    ngOnInit() {


        this.getParameters();


    }



    constructor(public navCtrl: NavController, public menuCtrl: MenuController, public service: settingService) {
        this.selectedSection = "tabButtonOne";



    }
    getParameters = function () {
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        this.service.getCurrentDeviceStaus(this.cus_id).subscribe(

            data => {
                this.device_id = data[0].current_device;
                this.user.device_id = data[0].current_device;
                console.log(this.device_id);
                this.getAllSettingData();
            },
            err => console.log(err)
        )
        for (var i = 0; i < 999;) {

            if (i == 5 && this.dry_run == false) {
                this.dry_run = true;
                this.dryRuncanArray.push(1);
                continue;
            }
            this.lowVoltageArray.push(i);
            this.lowVoltage3phaseArray.push(i);
            this.dryRuncanArray.push(i);
            i = i + 5;


        }
        for (var j = 0; j < 999;) {

            this.highVoltageArray.push((j));


            j = j + 5;
        }
        for (var l = 0; l < 999;) {

            this.highVoltage3phaseArray.push((l));



            l = l + 5;
        }

        for (var k = 0; k < 99.9;) {
            this.phase2DryRunArray.push(k.toFixed(1));

            k = k + 0.1;
        }
        for (var p = 0; p <= 99;) {
            this.differVoltageArray.push(p);
            this.lowVoltageScanArray.push(p);
            p = p + 1;

        }
        for (var m = 0; m <= 15;) {
            this.phase3DifferCurArray.push(m.toFixed(1));
            m = m + 0.1;
        }
        for (var s = 0; s <= 9; s++) {
            this.ScanDryRestart.push(s);
        }
    }
    selectHighVoltValue = function (data) {
        this.lowVoltageArray.length = 0;
        for (var i = 0; i < 999;) {
            if (data.highVolt > i) {
                this.lowVoltageArray.push(i);

            }
            i = i + 5;
        }
        if (data.lowVolt != '' && data.highVolt != '' && data.dryRun != '' && data.overLoad != '') {
            this.SavePhase2 = false;
        }
    }

    selectLowVoltValue = function (data) {

        this.highVoltageArray.length = 0;
        for (var i = 0; i < 999;) {
            if (data.lowVolt < i) {
                this.highVoltageArray.push((i));

            }
            i = i + 5;
        }
        if (data.lowVolt != '' && data.highVolt != '' && data.dryRun != '' && data.overLoad != '') {
            this.SavePhase2 = false;
        }

    }
    selectDryRun = function (data) {

        if (data.lowVolt != '' && data.highVolt != '' && data.dryRun != '' && data.overLoad != '') {
            this.SavePhase2 = false;
        }
    }
    select3PhaseHighVolt = function (data) {
        this.lowVoltage3phaseArray.length = 0;
        for (var i = 0; i < 999;) {
            if (data.highVolt > i) {
                this.lowVoltage3phaseArray.push(i);

            }
            i = i + 5;
        }
        if (data.lowVolt != '' && data.highVolt != '' && data.differVolt != '' && data.dryCur != '' && data.dryLoad != '' && data.differCur != '' && data.faultCt != '') {
            this.SavePhase3 = false;
        }
    }
    select3PhaseLowVolt = function (data) {
        this.highVoltage3phaseArray.length = 0;
        for (var i = 0; i < 999;) {
            if (data.lowVolt < i) {
                this.highVoltage3phaseArray.push((i));

            }
            i = i + 5;
        }

        if (data.lowVolt != '' && data.highVolt != '' && data.differVolt != '' && data.dryCur != '' && data.dryLoad != '' && data.differCur != '' && data.faultCt != '') {
            this.SavePhase3 = false;
        }
    }
    getAllSettingData = function () {

        this.service.getscannertimer(this.cus_id, this.user.device_id).subscribe(

            data => {

                if (data.length != 0) {
                    var dryrunscanstamp = data[0].dry_run_scan_time;
                    var dryhrs = (dryrunscanstamp).substr(0, 2);
                    var drymins = (dryrunscanstamp).substr(3, 2);
                    var drysec = (dryrunscanstamp).substr(6, 2);
                    var dry_tot_sec = parseInt(drymins) * 60 + parseInt(drysec);


                    var overloadscanstamp = data[0].over_load_scan_time;
                    var overhrs = overloadscanstamp.substr(0, 2);
                    var overmins = overloadscanstamp.substr(3, 2);
                    var oversec = overloadscanstamp.substr(6, 2);

                    var over_tot_sec = parseInt(overmins) * 60 + parseInt(oversec);


                    var voltscanstamp = data[0].volt_scan_time;
                    var volthrs = voltscanstamp.substr(0, 2);
                    var voltmins = voltscanstamp.substr(3, 2);
                    var voltsec = voltscanstamp.substr(6, 2);

                    var volt_tot_sec = parseInt(voltmins) * 60 + parseInt(voltsec);


                    var starscanstamp = data[0].start_delta_time;
                    var starhrs = starscanstamp.substr(0, 2);
                    var starmins = starscanstamp.substr(3, 2);
                    var starsec = starscanstamp.substr(6, 2);

                    var star_tot_sec = parseInt(starmins) * 60 + parseInt(starsec);


                    var startscanstamp = data[0].start_capacitor_time;
                    var starthrs = startscanstamp.substr(0, 2);
                    var startmins = startscanstamp.substr(3, 2);
                    var startsec = startscanstamp.substr(6, 2);

                    var start_tot_sec = parseInt(startmins) * 60 + parseInt(startsec);


                    var autoscanstamp = data[0].auto_start_time;
                    var autohrs = autoscanstamp.substr(0, 2);
                    var automins = autoscanstamp.substr(3, 2);
                    var autosec = autoscanstamp.substr(6, 2);

                    var auto_tot_mins = parseInt(autohrs) * 60 + parseInt(automins);


                    var dryscanstamp = data[0].dry_run_restart;
                    var dryhrs1 = dryscanstamp.substr(0, 2);
                    var drymins1 = dryscanstamp.substr(3, 2);
                    var drysec1 = dryscanstamp.substr(6, 2);

                    var dry_tot_mins = parseInt(dryhrs1) * 60 + parseInt(drymins1);



                    if (data.length > 0) {
                        this.user = {
                            cus_id: data[0].cus_id,
                            device_id: data[0].device_id,
                            dryScan: dry_tot_sec,
                            overLoad: over_tot_sec,
                            lowVolt: volt_tot_sec,
                            startDelta: star_tot_sec,
                            startCapacit: start_tot_sec,
                            autoStart: auto_tot_mins,
                            dryRun: dry_tot_mins,
                            dryRestarts: data[0].dry_run_restart_nos
                        }
                    }
                }
                else if (data.length == 0) {

                    this.SaveUserSet = true;
                }
            },
            err => console.log(err)
        )
        this.service.getParam2phase(this.cus_id, this.user.device_id).subscribe(

            data => {
                if (data.length > 0) {

                    this.phase2_par = {
                        cus_id: data[0].cus_id,
                        device_id: data[0].device_id,
                        lowVolt: data[0].low_volt,
                        highVolt: data[0].high_volt,
                        dryRun: data[0].dry_run_cur,
                        overLoad: data[0].over_load_cur
                    };
                } else if (data.length == 0) {

                    this.SavePhase2 = true;
                }
            },
            err => console.log(err)
        )
        this.service.getParam3phase(this.cus_id, this.user.device_id).subscribe(

            data => {

                if (data.length > 0) {


                    this.phase2_3Phasepar = {
                        cus_id: data[0].cus_id,
                        device_id: data[0].device_id,
                        lowVolt: data[0].low_volt,
                        highVolt: data[0].high_volt,
                        differVolt: data[0].diff_volt,
                        dryCur: data[0].dry_run_cur,
                        dryLoad: data[0].over_load_cur,
                        differCur: data[0].diff_cur,
                        faultCt: data[0].fault_ct

                    }
                } else if (data.length == 0) {

                    this.SavePhase3 = true;
                }
            },
            err => console.log(err)
        )


    }

    userClick = function () {
        this.selectedSection = "tabButtonOne";
        this.motorSetting = false;
        this.userSetting = true;
        this.phase1 = true;
        this.phase2 = false;


    }
    motorClick = function () {
        this.selectedSection = "tabButtonThree";
        this.motorSetting = true;
        this.userSetting = false;
        this.phase1 = true;
        this.phase2 = false;

    }
    phase1Click = function () {

        this.phase1 = true;
        this.phase2 = false;

    }
    phase2Click = function () {

        this.phase1 = false;
        this.phase2 = true;

    }
    selectlowVolt = function (data) {

        if (data.lowVolt != '' && data.highVolt != '' && data.dryRun != '' && data.overLoad != '') {
            this.SavePhase2 = false;
        }

    }
    selectHighVolt = function (data) {
        if (data.lowVolt != '' && data.highVolt != '' && data.dryRun != '' && data.overLoad != '') {
            this.SavePhase2 = false;
        }

    }

    selectOverLoad = function (data) {
        if (data.lowVolt != '' && data.highVolt != '' && data.dryRun != '' && data.overLoad != '') {
            this.SavePhase2 = false;
        }

    }
    Clear = function (data) {

        this.phase2_par = {

            lowVolt: '',
            highVolt: '',
            dryRun: '',
            overLoad: ''
        };
        if (data.lowVolt != '' && data.highVolt != '' && data.dryRun != '' && data.overLoad != '') {
            this.SavePhase2 = true;
        }

    }
    phase2Save = function (data) {
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        alert('Update Successfully !!!!');
        this.service.set2PhaseValue(this.cus_id, this.device_id, data).subscribe(

            data => {
                alert('Update Successfully !!!!');
            },
            err => console.log(err)
        )

    }
    phase3Save = function (data) {
        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        alert('Update Successfully !!!!');
        data.device_id = this.device_id;

        this.Phase3par = [
            {
                cus_id: this.cus_id,
                device_id: this.device_id,
                low_volt: data.lowVolt,
                high_volt: data.highVolt,
                diff_volt: data.differVolt,
                dry_run_cur: data.dryCur,
                over_load_cur: data.dryLoad,
                diff_cur: data.differCur,
                fault_ct: data.faultCt

            }]
        this.service.set3PhaseValue(this.Phase3par).subscribe(

            data => {
                alert('Update Successfully !!!!');
            },
            err => console.log(err)
        )

    }
    saveUserSettting = function (data) {

        this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
        this.device_id = this.device_id;
        alert('Update Successfully !!!!');
        //----------------DryRun Scan TIme-------------//
        var a = Math.trunc(data.dryScan / 60).toString();
        a = "0" + a;
        var b = (data.dryScan % 60).toString();
        if ((data.dryScan % 60) < 9) {
            b = "0" + b;
        }
        var drystamp = "00:" + a + ":" + b;
        // alert(drystaamp);
        //-------------------------------------------------//

        //----------------OverLoad Scan TIme-------------//
        var overmin = Math.trunc(data.overLoad / 60).toString();
        overmin = "0" + overmin;
        var oversec = (data.overLoad % 60).toString();
        if ((data.overLoad % 60) < 9) {
            oversec = "0" + oversec;
        }
        var overstamp = "00:" + overmin + ":" + oversec;
        // alert(overstaamp);
        //-------------------------------------------------//
        //----------------Lowvolt Scan TIme-------------//
        var lowmin = Math.trunc(data.lowVolt / 60).toString();
        lowmin = "0" + lowmin;
        var lowsec = (data.lowVolt % 60).toString();
        if ((data.lowVolt % 60) < 9) {
            lowsec = "0" + lowsec;
        }
        var lowstamp = "00:" + lowmin + ":" + lowsec;
        // alert(lowstaamp);
        //-------------------------------------------------//
        //----------------Stardelta Scan TIme-------------//
        var starmin = Math.trunc(data.startDelta / 60).toString();
        starmin = "0" + starmin;
        var starsec = (data.startDelta % 60).toString();
        if ((data.startDelta % 60) < 9) {
            starsec = "0" + starsec;
        }
        var starstamp = "00:" + starmin + ":" + starsec;
        // alert(starstaamp);
        //-------------------------------------------------//

        //----------------StartingCap Scan TIme-------------//
        var starcapmin = Math.trunc(data.startCapacit / 60).toString();
        starcapmin = "0" + starcapmin;
        var starcapsec = (data.startCapacit % 60).toString();
        if ((data.startDelta % 60) < 9) {
            starcapsec = "0" + starcapsec;
        }
        var startcapstamp = "00:" + starcapmin + ":" + starcapsec;
        // alert(starstaamp);
        //-------------------------------------------------//

        //----------------autoStart Scan TIme-------------//
        var autostarthrs = Math.trunc(data.autoStart / 60).toString();
        autostarthrs = "0" + autostarthrs;
        var autostartmin = (data.autoStart % 60).toString();
        if ((data.startDelta % 60) < 9) {
            autostartmin = "0" + autostartmin;
        }
        var autostartstamp = autostarthrs + ":" + autostartmin + ":00";
        // alert(autostartstaamp);
        //-------------------------------------------------//

        //----------------DryRunStart Scan TIme-------------//
        var dryrunmin = Math.trunc(data.dryRun / 60).toString();
        dryrunmin = "0" + dryrunmin;
        var dryrunsec = (data.dryRun % 60).toString();
        if ((data.startDelta % 60) < 9) {
            dryrunsec = "0" + dryrunsec;
        }
        var dryrunstamp = dryrunmin + ":" + dryrunsec + ":00";
        // alert(starstaamp);
        //-------------------------------------------------//



        this.userScanTimer = [
            {

                cus_id: this.cus_id,
                device_id: this.device_id,
                dry_run_scan_time: drystamp,
                over_load_scan_time: overstamp,
                volt_scan_time: lowstamp,
                start_delta_time: starstamp,
                start_capacitor_time: startcapstamp,
                auto_start_time: autostartstamp,
                dry_run_restart: dryrunstamp,
                dry_run_restart_nos: data.dryRestarts

            }];

        this.service.setScannerTimer(this.userScanTimer).subscribe(

            data => {
                let alert = data[0];
                alert(alert);
            },
            err => console.log(err)
        )
    }


    select3PhasedifferVolt = function (data) {

        if (data.lowVolt != '' && data.highVolt != '' && data.differVolt != '' && data.dryCur != '' && data.dryLoad != '' && data.differCur != '' && data.faultCt != '') {
            this.SavePhase3 = false;
        }
    }
    select3PhasedryCur = function (data) {

        if (data.lowVolt != '' && data.highVolt != '' && data.differVolt != '' && data.dryCur != '' && data.dryLoad != '' && data.differCur != '' && data.faultCt != '') {
            this.SavePhase3 = false;
        }
    }
    select3PhasedryLoad = function (data) {
        if (data.lowVolt != '' && data.highVolt != '' && data.differVolt != '' && data.dryCur != '' && data.dryLoad != '' && data.differCur != '' && data.faultCt != '') {
            this.SavePhase3 = false;
        }
    }
    select3PhasedifferCur = function (data) {

        if (data.lowVolt != '' && data.highVolt != '' && data.differVolt != '' && data.dryCur != '' && data.dryLoad != '' && data.differCur != '' && data.faultCt != '') {
            this.SavePhase3 = false;
        }

    }
    select3PhasefaultCt = function (data) {
        if (data.lowVolt != '' && data.highVolt != '' && data.differVolt != '' && data.dryCur != '' && data.dryLoad != '' && data.differCur != '' && data.faultCt != '') {
            this.SavePhase3 = false;
        }

    }
    clear3Phase = function (data) {

        this.phase2_3Phasepar = {

            lowVolt: '',
            highVolt: '',
            differVolt: '',
            dryCur: '',
            dryLoad: '',
            differCur: '',
            faultCt: ''

        }
        if (data.lowVolt != '' && data.highVolt != '' && data.differVolt != '' && data.dryCur != '' && data.dryLoad != '' && data.differCur != '' && data.faultCt != '') {
            this.SavePhase3 = true;
        }

    }
    selectdryScan = function (data) {
        if (data.dryScan != '' && data.overLoad != '' && data.lowVolt != '' && data.startDelta != '' && data.startCapacit != '' && data.autoStart != '' && data.dryRun != '' && data.dryRestart != '') {
            this.SaveUserSet = false;
        }
    }
    selectoverLoad = function (data) {
        if (data.dryScan != '' && data.overLoad != '' && data.lowVolt != '' && data.startDelta != '' && data.startCapacit != '' && data.autoStart != '' && data.dryRun != '' && data.dryRestart != '') {
            this.SaveUserSet = false;
        }

    }

    selectstartDelta = function (data) {
        if (data.dryScan != '' && data.overLoad != '' && data.lowVolt != '' && data.startDelta != '' && data.startCapacit != '' && data.autoStart != '' && data.dryRun != '' && data.dryRestart != '') {
            this.SaveUserSet = false;
        }

    }
    selectstartCapacit = function (data) {
        if (data.dryScan != '' && data.overLoad != '' && data.lowVolt != '' && data.startDelta != '' && data.startCapacit != '' && data.autoStart != '' && data.dryRun != '' && data.dryRestart != '') {
            this.SaveUserSet = false;
        }

    }
    selectautoStart = function (data) {
        if (data.dryScan != '' && data.overLoad != '' && data.lowVolt != '' && data.startDelta != '' && data.startCapacit != '' && data.autoStart != '' && data.dryRun != '' && data.dryRestart != '') {
            this.SaveUserSet = false;
        }

    }
    selectdryRun = function (data) {
        if (data.dryScan != '' && data.overLoad != '' && data.lowVolt != '' && data.startDelta != '' && data.startCapacit != '' && data.autoStart != '' && data.dryRun != '' && data.dryRestart != '') {
            this.SaveUserSet = false;
        }

    }
    selectdryRestart = function (data) {
        if (data.dryScan != '' && data.overLoad != '' && data.lowVolt != '' && data.startDelta != '' && data.startCapacit != '' && data.autoStart != '' && data.dryRun != '' && data.dryRestart != '') {
            this.SaveUserSet = false;
        }

    }
    clearUserSettting = function (data) {

        this.user = {

            dryScan: '',
            overLoad: '',
            lowVolt: '',
            startDelta: '',
            startCapacit: '',
            autoStart: '',
            dryRun: '',
            dryRestart: ''
        }
        if (data.dryScan != '' && data.overLoad != '' && data.lowVolt != '' && data.startDelta != '' && data.startCapacit != '' && data.autoStart != '' && data.dryRun != '' && data.dryRestart != '') {
            this.SaveUserSet = true;
        }
    }


}
