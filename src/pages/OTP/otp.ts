import { Component } from "@angular/core";
import { NavController, MenuController } from "ionic-angular";
import { HomePage } from "../home/home";
import { OtpService } from "./otp.service";

@Component({
    selector: 'Verify-OTP',
    templateUrl: 'otp.html',
    providers: [OtpService]
})

export class AppVerifyOTP {

    otp: any;
    mobileNum_storage: any;
    valid: boolean = true;

    constructor(public navctrl: NavController, public menuctrl: MenuController, private service_otp: OtpService) {

        this.mobileNum_storage = localStorage.getItem('mobileNumber');

    }

    ionViewDidEnter() {
        // the root left menu should be disabled on this page
        this.menuctrl.enable(false);
    }

    ionViewWillLeave() {
        // enable the root left menu when leaving this page
        this.menuctrl.enable(true);
    }


    sendOTP(OTP) {
        this.service_otp.checkOTP(this.mobileNum_storage, OTP).subscribe(

            data => {
                this.otp = data;
                localStorage.setItem("customer_device_id", data[0].current_device);
                localStorage.setItem("customer_id", data[0].cus_id)
                this.navigateTo(this.otp);
            }
        )
    }
    navigateTo(status) {
        if (status[1].message == "success") {
            localStorage.setItem("dev_ids", status[0].current_device);
            localStorage.setItem("cus_ids", status[0].cus_id);
            localStorage.setItem("cus_name", status[0].cus_name);
            localStorage.setItem("loggedIn", 'true');
            this.navctrl.push(HomePage);

        }

        else if (status[1].message == "fail") {

            this.valid = false;

        }
    }
}
