import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class OtpService {

    // ip = "http://52.15.171.241:3111";
    // ip = "http://192.168.1.190:3111";
    ip = "http://103.14.120.213:3000";

    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }

    checkOTP(mobileNum, otp): Observable<any> {
        return this.http.get(this.ip + '/api/homepage/verify_otp?mobile=' + mobileNum + '&otp=' + otp).map(res => res.json());
    }
}
