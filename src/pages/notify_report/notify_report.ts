import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AppNotifyService } from './notify_report.service';



@Component({
    selector: 'Log-Page',
    templateUrl: 'notify_report.html',
    providers: [AppNotifyService]
})
export class AppNotifyReport {

    split_date_time: String;
    device_id; customer_id;
    logDetails = [];
    time_array = [];
    total_array = [];
    logdata: boolean = true;
    date;
    ngOnInit() {
        this.fetch_device_id();
    }



    constructor(public navCtrl: NavController, public menuCtrl: MenuController, private logservice: AppNotifyService) {

        this.customer_id = localStorage.getItem('cus_ids');
    }

    fetch_device_id() {
        this.logservice.getDeviceId(this.customer_id).subscribe(
            data => {
                this.device_id = data[0].current_device;
                this.report_details(this.device_id)
            },
            err => console.log(err)
        )
    }


    report_details(dev_id) {

        this.logservice.getlogDetails(dev_id).subscribe(

            data => {
                this.logDetails = data;
                console.log(data);
                if (this.logDetails.length == 0) {
                    this.logdata = true;
                }
                else {
                    this.logdata = false;
                }
                this.splitDateTime(this.logDetails);
            },
            err => {
                console.log(err)
            }
        )

    }
    splitDateTime(logdetails_array) {
        for (var i = 0; i < logdetails_array.length; i++) {
            this.split_date_time = logdetails_array[i].created_at.split("T");
            this.date = this.split_date_time[0];
            var zone_Time = this.split_date_time[1];
            var time = zone_Time.substr(0, 8);
            var type = logdetails_array[i].type;
            var message = logdetails_array[i].message;
            var res = { type: type, message: message, time: time };
            this.total_array.push(res);
        }
    }
}
