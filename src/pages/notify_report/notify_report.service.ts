import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class AppNotifyService {


    ip = "http://103.14.120.213:3000";
    data: any;

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }
    getDeviceId(customer_id): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/getCurrentDevice?cus_id=" + customer_id).map(res => res.json());
    }

    getlogDetails(device_id): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/get_notification?device_id=" + device_id).map(res => res.json());
    }

}
