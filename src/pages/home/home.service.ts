
import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';


@Injectable()

export class HomeService {

    // ip = "http://52.15.171.241:3111";
    // ip = "http://192.168.1.190:3111";
    ip = "http://103.14.120.213:3000";
    data: any

    constructor(private http: Http) {

        this.http = http;
        this.data = null;
    }

    details(deviceId): Observable<any> {

        return this.http.get(this.ip + "/api/homepage/getdashboardstatus?device_id=" + deviceId).map(res => res.json());
    }
    getModeDetails(device_id, customer_id): Observable<any> {
        return this.http.get(this.ip + "/api/homepage/getmodes?cus_id=" + customer_id + "&device_id=" + device_id).map(res => res.json());
    }
    getCurrentDeviceStaus(cus_id): Observable<any> {
       
        return this.http.get(this.ip + "/api/homepage/getCurrentDevice?cus_id=" + cus_id).map(res => res.json());
    }
}
