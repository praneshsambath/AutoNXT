import { Component, NgZone } from '@angular/core';
import { NavController, Refresher } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { HomeService } from './home.service';
import * as io from "socket.io-client";
import { LocalNotifications } from '@ionic-native/local-notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [HomeService]
})
export class HomePage {
  res: any;
  deviceId_storage: any;
  customerId_storage: any;
  details: string[];
  split = [];
  socket: any;
  cus_id;
  Valve1;
  rVolt: any;
  customer_device;
  dashboard_details;
  status: boolean = false;
  date: any;
  refresh = [];
  split_elect_time = [];
  mode_details = [];
  autoMode: boolean;
  mannualMode: boolean;
  autoSequence: boolean;
  autoTimer: boolean;
  sequence_status: boolean;
  zone: any;
  electricity_time: String;
  motor_time: String;
  valve_status: boolean;

  ngOnInit() {
    this.intialFunction();


  }
  constructor(public navCtrl: NavController, public menuCtrl: MenuController, public Service: HomeService, public notify: LocalNotifications) {
    // this.socket = io("http://52.15.171.241:3111");
    // this.socket = io("http://192.168.1.190:3111");
    this.socket = io("http://103.14.120.213:3000");
    this.zone = new NgZone({ enableLongStackTrace: false })

  }
  intialFunction() {

    this.date = new Date().toISOString();
    this.split = [];
    this.refresh = this.date;
    this.menuCtrl.enable(true, 'myMenu');
    this.customerId_storage = localStorage.getItem("cus_ids");
    this.deviceId_storage = localStorage.getItem("dev_ids")
    this.ModeDetails(this.deviceId_storage, this.customerId_storage);
    this.getDeviceId();

  }

  getDeviceId() {
    this.cus_id = JSON.parse(localStorage.getItem('cus_ids'));
    this.Service.getCurrentDeviceStaus(this.cus_id).subscribe(
      data => {
        this.Valve1 = data[0].current_device;
        this.dashboard(this.Valve1);
      },
      err => console.log(err)
    )
  }
  test(data) {
    this.notify.schedule({
      id: 1,
      text: data.type + " : " + data.message,
      data: data.time
    });
  }
  doRefresh(refresher) {
    this.intialFunction();
    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
  dashboard(deviceId) {
    this.socket.on('notify_' + deviceId, (data) => {
      this.zone.run(() => {
        this.test(data);

      })
    });

    this.Service.details(deviceId).subscribe(
      data => {
        if (data.length != 0) {
          this.dashboard_details = data[0];
          console.log(this.dashboard_details.valves)
          this.splitDate_time(this.dashboard_details.motor_start_time, this.dashboard_details.elect_time);

          if (this.dashboard_details.motor_status == '1') {
            this.status = true;
          }
          else {
            this.status = false;
          }
          if (this.dashboard_details.valves == "00" || this.dashboard_details.valves == "null") {
            this.valve_status = true;
          }
          else {
            this.valve_status = false;
          }
        }
      },
      err => console.log(err)
    )
  }
  splitDate_time(time_date, elect_time) {

    this.split = time_date.split("T");
    this.motor_time = this.split[1].substr(0, 8);
    this.split_elect_time = elect_time.split("T");
    this.electricity_time = this.split_elect_time[1].substr(0, 8);
  }
  refresh_splitTime() {
    this.refresh = this.date.split("T");
    this.intialFunction();
  }
  ModeDetails(dev_ids, cus_id) {
    this.Service.getModeDetails(dev_ids, cus_id).subscribe(
      data => {
        this.mode_details = data;
        if (data.length != 0) {
          if (data[0].manual_mode == 1) {
            this.mannualMode = true;
            this.autoMode = false;
          }
          else if (data[0].auto_mode == 1) {
            this.autoMode = true;
            this.mannualMode = false;
            if (data[0].auto_mode_timer == 1) {
              this.autoTimer = true;
              this.autoSequence = false;
            }
            else {
              this.autoTimer = false;
              this.autoSequence = true;
              if (data[0].auto_mode_sequence_status == 1) {
                this.sequence_status = true;
                this.autoTimer = false;
              }
              else {
                this.sequence_status = false;
                this.autoTimer = false;
              }
            }
          }
        }
      },
      err => console.log(err)
    )
  }
}